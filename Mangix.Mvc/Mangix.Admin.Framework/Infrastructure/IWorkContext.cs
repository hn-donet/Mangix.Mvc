﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mangix.Mapping.SysUser;
using Mangix.Mapping.SysStore;
using Mangix.Mapping.Category;

namespace Mangix.Admin.Framework.Infrastructure
{
    public interface IWorkContext
    {
        /// <summary>
        /// 当前用户
        /// </summary>
        SysUserMapping CurrentUser { get; }

        /// <summary>
        /// 系统对象
        /// </summary>
        SysStoreMapping Store { get; }

        /// <summary>
        /// 
        /// </summary>
        List<CategoryMapping> MyDescriperes { get; }
    }
}
