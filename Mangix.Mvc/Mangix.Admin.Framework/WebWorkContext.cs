﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mangix.Mapping.SysUser;
using Mangix.Admin.Framework.Security;
using Mangix.Mapping.SysStore;
using Mangix.Services.SysStore;
using Mangix.Mapping.Category;

namespace Mangix.Admin.Framework
{
    public class WebWorkContext : Infrastructure.IWorkContext
    {
        private IAuthenticationService _authenticationService;
        private ISysStoreService _sysStoreService;

        public WebWorkContext(IAuthenticationService authenticationService,
            ISysStoreService sysStoreService)
        {
            this._authenticationService = authenticationService;
            this._sysStoreService = sysStoreService;
        }

        /// <summary>
        /// 当前登录用户对象
        /// </summary>
        public SysUserMapping CurrentUser
        {
            get
            {
                return _authenticationService.CurrentUser;
            }
        }

        /// <summary>
        /// 系统对象
        /// </summary>
        public SysStoreMapping Store
        {
            get
            {
                return _sysStoreService.getStore();
            }
        }

        /// <summary>
        /// 我的权限数据
        /// </summary>
        public List<CategoryMapping> MyDescriperes
        {
            get
            {
                return _authenticationService.getMyDescirpter();
            }
        }
    }
}
