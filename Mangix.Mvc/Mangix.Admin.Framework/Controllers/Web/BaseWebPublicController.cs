﻿using Mangix.Admin.Framework.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mangix.Admin.Framework.Controllers.Web
{ 
    [PublicAntiForgery]
    [HandleException]
    public class BaseWebPublicController:BaseController
    {
    }
}
