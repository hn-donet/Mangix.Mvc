﻿using Mangix.Admin.Framework.Infrastructure;
using Mangix.Admin.Framework.Mvc.Filters; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Routing;

namespace Mangix.Admin.Framework.Controllers.Mobile
{
    [PublicAntiForgery]
    [HandleException]
    public abstract class BaseMobilePublicController:BaseMobileAreaController
    { 
        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestContext"></param>
        protected override void Initialize(RequestContext requestContext)
        {
            
            base.Initialize(requestContext);
        }
    }
}
