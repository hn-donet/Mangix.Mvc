﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;
using Mangix.Admin.Framework.Mvc.Filters;
using Mangix.Admin.Framework.Infrastructure;

namespace Mangix.Admin.Framework.Controllers.Admin
{
    [HttpsRequirement]
    [PublicAntiForgery]
    [UserLastActivity]
    [Permission]
    public abstract class BaseAdminController : BaseAdminPublicController
    {

    }
}
