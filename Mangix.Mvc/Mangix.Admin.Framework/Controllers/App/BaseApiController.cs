﻿using Mangix.Admin.Framework.Models; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Mangix.Admin.Framework.Controllers.App
{
    /// <summary>
    /// 
    /// </summary> 
    public abstract class BaseApiController:ApiController
    {
        /// <summary>
        /// api结果对象
        /// </summary>
        public ApiJsonResult ApiResult { get; }

        public BaseApiController()
        {
            ApiResult = new ApiJsonResult(-1);
        }
    }
}
