﻿using Mangix.Admin.Framework.Infrastructure; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Controllers;

namespace Mangix.Admin.Framework.Controllers.App
{
     
    public class BaseApiPublicController : BaseApiController
    { 
        /// <summary>
        /// 
        /// </summary>
        /// <param name="controllerContext"></param>
        protected override void Initialize(HttpControllerContext controllerContext)
        { 
            base.Initialize(controllerContext);
        }
    }
}
