﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Mangix.Admin.Framework.Menu;
using Mangix.Core.Caching;
using Mangix.Core.Librs;
using Mangix.Entities;
using Mangix.Mapping.SysUser;
using Mangix.Services.SysPermission;
using Mangix.Services.SysUser;
using Mangix.Services.SysUserRole;
using Mangix.Services.SysUserToken;
using Mangix.Mapping.Category;
using Mangix.Services.Category;

namespace Mangix.Admin.Framework.Security
{
    /// <summary>
    /// 
    /// </summary>
    public class AuthenticationService : IAuthenticationService
    {
        private ISysUserTokenService _sysUserTokenService;
        private ISysUserRegistrationService _sysUserRegistrationService;
        private ICacheManager _cacheManager;
        private ISysUserService _sysUserService;
        private ISysUserRoleService _sysUserRoleService;
        private ISysPermissionService _sysPermissionService;
        private ICategoryService _categoryService;

        public AuthenticationService(ISysUserTokenService sysUserTokenService,
            ISysUserRegistrationService sysUserRegistrationService,
            ISysUserService sysUserService,
            ICacheManager cacheManager,
            ISysPermissionService sysPermissionService,
            ICategoryService categoryService,
            ISysUserRoleService sysUserRoleService)
        {
            this._sysUserTokenService = sysUserTokenService;
            this._sysUserRegistrationService = sysUserRegistrationService;
            this._cacheManager = cacheManager;
            this._sysUserService = sysUserService;
            this._sysUserRoleService = sysUserRoleService;
            this._sysPermissionService = sysPermissionService;
            this._categoryService = categoryService;
            this.CurrentUser = getAuthenticatedSysUser();
        }

        /// <summary>
        /// 进入系统
        /// </summary>
        /// <param name="user"></param>
        /// <param name="isPersistent"></param>
        public void signIn(SysUser user, bool isPersistent)
        {
            Entities.SysUserToken userToken = new Entities.SysUserToken()
            {
                Id = Guid.NewGuid(),
                SysUserId = user.Id,
                ExpireTime = DateTime.Now.Add(FormsAuthentication.Timeout)
            };
            _sysUserTokenService.insertToken(userToken);

            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(2,
                user.Account,
                DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout),
                isPersistent,
                userToken.Id.ToString(),
                FormsAuthentication.FormsCookiePath);
            string ticketValue = FormsAuthentication.Encrypt(ticket);
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, ticketValue);
            cookie.HttpOnly = true;
            if (ticket.IsPersistent)
                cookie.Expires = ticket.Expiration;
            cookie.Secure = FormsAuthentication.RequireSSL;
            cookie.Path = FormsAuthentication.FormsCookiePath;
            if (FormsAuthentication.CookieDomain != null)
                cookie.Domain = FormsAuthentication.CookieDomain;
            CookieHelper.Add(cookie);
        }

        /// <summary>
        /// 退出登录
        /// </summary>
        public void signOut()
        {
            FormsAuthentication.SignOut();
        }

        /// <summary>
        /// 当前登录人对象
        /// </summary>
        public SysUserMapping CurrentUser { get; }

        /// <summary>
        /// 获取验证通过已登录的用户信息
        /// </summary>
        /// <returns></returns>
        private SysUserMapping getAuthenticatedSysUser()
        {
            if (!isAuthenticated())
                return null;
            var identity = (FormsIdentity)HttpContext.Current.User.Identity;
            string token_id = identity.Ticket.UserData;
            if (String.IsNullOrEmpty(token_id))
                throw new Exception("ticket.UserData 为空");
            Guid tokenId = Guid.Empty;
            if (Guid.TryParse(token_id, out tokenId))
            {
                var token_item = _sysUserTokenService.getLoggedToken(tokenId);
                if (token_item != null)
                    return _sysUserService.getLoggedUserById(token_item.SysUserId);
            }
            return null;
        }

        /// <summary>
        /// 判断是否存在登录票据
        /// </summary>
        /// <returns></returns>
        public bool isAuthenticated()
        {
            return HttpContext.Current != null
                && HttpContext.Current.Request != null
                && HttpContext.Current.Request.IsAuthenticated
                && (HttpContext.Current.User.Identity is FormsIdentity);
        }

        /// <summary>
        /// 权限验证
        /// </summary>
        /// <param name="filterContext"></param>
        /// <returns></returns>
        public bool authorize(ActionExecutingContext filterContext)
        {
            string controller = (string)filterContext.RouteData.Values["controller"];
            string action = (string)filterContext.RouteData.Values["action"];
            return authorize(action, controller);
        }

        /// <summary>
        /// 权限判断
        /// </summary>
        /// <param name="action"></param>
        /// <param name="controller"></param>
        /// <returns></returns>
        public bool authorize(string action, string controller)
        {
            var user = CurrentUser;
            if (user.IsAdmin)
                return true;
            var list = getMyDescirpter();
            if (list == null)
                return false;
            return list.Any(o => o.Action != null && o.Controller != null &&
                o.Controller.Equals(controller, StringComparison.InvariantCultureIgnoreCase) && o.Action.Equals(action));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<CategoryMapping> getMyDescirpter()
        {
            var user = CurrentUser;
            if (user == null)
                return null;

            var list = _categoryService.getAllCache();
            if (list == null)
                return null;
            if (user.IsAdmin)
                return list;
            //获取角色
            var roleList = _sysUserRoleService.getRoleByUserId(user.Id);
            if (roleList == null || !roleList.Any())
                return null;
            //角色权限
            var perm_list = _sysPermissionService.getByRoleIds(roleList.Select(x => x.Id).ToList());
            if (perm_list == null || !perm_list.Any())
                return null;
            return list.Join(perm_list, a => a.Id, b => b.CategoryId, (a, b) => a).Distinct().ToList();

        }
    }
}
