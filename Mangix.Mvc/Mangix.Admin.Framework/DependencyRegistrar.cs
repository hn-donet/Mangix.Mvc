﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Mangix.Core.Infrastructure;
using System.Data.Entity;
using Mangix.Data;
using Mangix.Core.Data;
using Mangix.Core.Caching;
using Autofac.Integration.Mvc;
using System.Reflection;
using Mangix.Admin.Framework.Security;
using System.Web;

namespace Mangix.Admin.Framework
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        public void Register(ContainerBuilder builder)
        {
            builder.RegisterControllers(Assembly.Load("Mangix.Admin"));
            builder.Register<DbContext>(x=>new Entities.MangixEntities()).InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(EfRepository<>)).As(typeof(IRepository<>)).InstancePerLifetimeScope();
            builder.RegisterType<MemoryCacheManager>().As<ICacheManager>().InstancePerLifetimeScope();
            //
            var assembly = Assembly.Load("Mangix.Services");
            builder.RegisterAssemblyTypes(assembly).AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly()).AsImplementedInterfaces().InstancePerLifetimeScope();

            builder.RegisterType<AuthenticationService>().As<IAuthenticationService>().InstancePerLifetimeScope();
        }
    }
}
