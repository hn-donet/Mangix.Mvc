﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mangix.Core.Extensions;

namespace Mangix.Admin.Framework.Models
{
    /// <summary>
    /// api全局返回数据
    /// </summary>
    public class ApiJsonResult
    {
        public ApiJsonResult(int code = 0)
        {
            this.code = code;
            switch (this.code)
            {
                case 1001:
                    msg = "服务器错误";
                    break;
                case 1002:
                    msg = "未登录";
                    break;
                case 1003:
                    msg = "未知的请求源";
                    break;
                case 1004:
                    msg = "token过期";
                    break;
                case 1005:
                    msg = "参数不正确";
                    break;

                case 10010:
                    msg = "接口维护";
                    break;
                case 10011:
                    msg = "接口停用";
                    break;
                case 0:
                    msg = "成功";
                    break;
                case -1:
                    msg = "未知错误";
                    break;
                default:
                    msg = "不可描述";
                    break;
                    
            }
            data = new object(); 
        }

        /// <summary>
        /// -1：未知错误
        /// 0:正确 
        /// 1001：服务器错误，
        /// 1002：未登录，
        /// 1003：未知的请求源，
        /// 1004：请求超时，
        /// 1005：参数不正确
        /// 
        /// 10010：接口维护，
        /// 10011：接口停用
        /// </summary>
        public int code { get; set; }

        /// <summary>
        /// 提示信息
        /// </summary>
        public string msg { get; set; }

        /// <summary>
        /// 返回结果
        /// </summary>
        public object data { get; set; }
         
    }
}
