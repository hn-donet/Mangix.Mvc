﻿using Mangix.Mapping.Setting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mangix.Services.Setting
{
    public interface ISettingService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<SettingMapping> getAllCache();

        /// <summary>
        /// 通过名称获取
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        SettingMapping getByName(string name);
         

    }
}
