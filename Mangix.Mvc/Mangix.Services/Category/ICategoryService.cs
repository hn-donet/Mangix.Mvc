﻿using Mangix.Mapping.Category;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mangix.Services.Category
{
    public interface ICategoryService
    { 
        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="appkey"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        void initCategory(List<CategoryMapping> list);

        /// <summary>
        /// 获取所有数据并缓存
        /// </summary>
        /// <returns></returns>
        List<CategoryMapping> getAllCache();

        /// <summary>
        /// 禁用菜单功能
        /// </summary>
        /// <param name="id"></param>
        Tuple<bool,string> disabledOrNoCategory(int id);
    }
}
