﻿using Mangix.Core.Caching;
using Mangix.Core.Data;
using Mangix.Mapping;
using Mangix.Mapping.Category;
using Mangix.Services.ActivityLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mangix.Services.Category
{
    public class CategoryService : ICategoryService
    {
        private const string MODEL_KEY = "Mangix.services.category_all";
        private IRepository<Entities.Category> _categoryRepository;
        private IRepository<Entities.SysPermission> _permissionRepository;
        private IActivityLogService _activityLogService;
        private ICacheManager _cacheManager;

        public CategoryService(IRepository<Entities.Category> categoryRepository,
            IRepository<Entities.SysPermission> permissionRepository,
            ICacheManager cacheManager,
            IActivityLogService activityLogService)
        {
            this._categoryRepository = categoryRepository;
            this._permissionRepository = permissionRepository;
            this._cacheManager = cacheManager;
            this._activityLogService = activityLogService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<CategoryMapping> getAllCache()
        {
            return _cacheManager.get<List<CategoryMapping>>(MODEL_KEY, () =>
            {
                var list = _categoryRepository.Table.Select(item => new CategoryMapping()
                {
                    Id = item.Id,
                    Name = item.Name,
                    RouteName = item.RouteName,
                    Action = item.Action,
                    Controller = item.Controller,
                    CssClass = item.CssClass,
                    FatherResource = item.FatherResource,
                    IsMenu = item.IsMenu,
                    Sort = item.Sort,
                    SysResource = item.SysResource,
                    FatherID = item.FatherID,
                    IsDisabled = item.IsDisabled,
                    ResouceID = item.ResouceID
                }).ToList();
                if (list.Any())
                    return list;
                return null;
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appkey"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public void initCategory(List<CategoryMapping> list)
        {
            var oldList = _categoryRepository.Table.ToList();
            oldList.ForEach(del =>
            {
                var item = list.FirstOrDefault(o => o.SysResource == del.SysResource);
                if (item == null)
                {
                    var permissionList = del.SysPermission.ToList();
                    permissionList.ForEach(delPrm =>
                    {
                        _permissionRepository.Entities.Remove(delPrm);
                    });
                    _categoryRepository.Entities.Remove(del);
                }
            });
            list.ForEach(entity =>
            {
                var item = oldList.FirstOrDefault(o => o.SysResource == entity.SysResource);
                if (item == null)
                {
                    _categoryRepository.Entities.Add(entity.toEntity());
                }
                else
                {
                    item.Action = entity.Action;
                    item.Controller = entity.Controller;
                    item.CssClass = entity.CssClass;
                    item.FatherResource = entity.FatherResource;
                    item.IsMenu = entity.IsMenu;
                    item.Name = entity.Name;
                    item.RouteName = entity.RouteName;
                    item.SysResource = entity.SysResource;
                    item.Sort = entity.Sort;
                    item.FatherID = entity.FatherID;
                    item.ResouceID = entity.ResouceID;
                }
            });
            if (_categoryRepository.DbContext.ChangeTracker.HasChanges())
                _categoryRepository.DbContext.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        public Tuple<bool, string> disabledOrNoCategory(int id)
        {
            var item = _categoryRepository.getById(id);
            if (item == null)
                return new Tuple<bool, string>(false, "数据不存在");
            item.IsDisabled = !item.IsDisabled;
            _categoryRepository.update(item);
            _activityLogService.entityUpdated(item);
            _cacheManager.remove(MODEL_KEY);
            return new Tuple<bool, string>(true, "保存成功");
        }



    }
}
