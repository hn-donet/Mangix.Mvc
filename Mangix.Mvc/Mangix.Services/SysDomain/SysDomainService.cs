﻿using Mangix.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using Mangix.Core.Caching;
using Mangix.Mapping.SysDomain;
using Mangix.Mapping;
using Microsoft.Web.Administration;

namespace Mangix.Services.SysDomain
{
    public class SysDomainService : ISysDomainService
    {
        private const string MODEL_KEY = "Baobei.service.sysdomain";
        private IRepository<Entities.SysDomain> _sysDomainRepository;
        private ICacheManager _cacheManager;

        public SysDomainService(IRepository<Entities.SysDomain> sysDomainRepository,
            ICacheManager cacheManager)
        {
            this._sysDomainRepository = sysDomainRepository;
            this._cacheManager = cacheManager;
        }

        /// <summary>
        /// 获取所有数据病换成
        /// </summary>
        /// <returns></returns>
        public List<SysDomainMapping> getAllCache()
        {
            return _cacheManager.get<List<SysDomainMapping>>(MODEL_KEY, () =>
             {
                 var result = _sysDomainRepository.Table.ToList();
                 if (result.Any())
                 {
                     return result.Select(item => item.toModel()).ToList();
                 }
                 return null;
             });

        }

        /// <summary>
        /// 添加域名并设置iis域名
        /// </summary>
        /// <param name="model"></param>
        public void insertDomain(SysDomainMapping model)
        {
            using (ServerManager sm = new ServerManager())
            {
                var siteName = System.Web.Hosting.HostingEnvironment.SiteName;
                var site = sm.Sites[siteName];
                //删除旧域名
                var old_list = _sysDomainRepository.Table.Where(o => o.DomainType == model.DomainType && o.Domain != model.Domain).ToList();
                old_list.ForEach(del =>
                {
                    string del_info = "*:80:" + del.Domain.ToLower();
                    var binding = site.Bindings.FirstOrDefault(o => o.BindingInformation.Equals(del_info, StringComparison.InvariantCultureIgnoreCase));
                    if (binding != null)
                    {
                        site.Bindings.Remove(binding);
                        sm.CommitChanges();
                    }
                    _sysDomainRepository.Entities.Remove(del);
                });

                string info = "*:80:" + model.Domain.ToLower();
                if (!site.Bindings.Any(o => o.BindingInformation.Equals(info, StringComparison.InvariantCultureIgnoreCase)))
                {
                    site.Bindings.Add(info, "http");
                    sm.CommitChanges();
                }
                if (!_sysDomainRepository.Table.Any(o => o.Domain == model.Domain))
                {
                    var item = model.toEntity();
                    _sysDomainRepository.Entities.Add(item);
                }
                _sysDomainRepository.DbContext.SaveChanges();
                _cacheManager.remove(MODEL_KEY);
            }
        }

    }
}
