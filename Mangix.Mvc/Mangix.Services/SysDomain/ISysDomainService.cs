﻿using Mangix.Mapping.SysDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mangix.Services.SysDomain
{
    public interface ISysDomainService
    {
        /// <summary>
        /// 获取全部数据并缓存
        /// </summary>
        /// <returns></returns>
        List<SysDomainMapping> getAllCache();

        /// <summary>
        /// 添加并绑定iis域名
        /// </summary>
        /// <param name="model"></param>
        void insertDomain(SysDomainMapping model);
          
    }
}
