﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Mangix.Core.Extensions
{
    /// <summary>
    /// 字符串扩展
    /// </summary>
    public static class StringExtension
    {
        /// <summary>
        /// 去除 任何空白字符
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string trimSpace(this string source)
        {
            return Regex.Replace(source, @"\s", "");
        }

        /// <summary>
        /// 验证是否是手机号码
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static bool isPhone(this string source)
        {
            Regex reg = new Regex(@"^1[345678]\d{9}$");
            return reg.IsMatch(source);
        }















    }
}
