﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mangix.Mapping.SysPermission
{
    [Serializable]
    public class PermissionRecordMapping
    {
        /// <summary>
		/// Id
        /// </summary>		
        public Guid Id { get; set; }

        /// <summary>
        /// 
        /// </summary>		
        public int CategoryId { get; set; }

        /// <summary>
        /// RoleId
        /// </summary>		
        public Guid RoleId { get; set; }

        /// <summary>
        /// Creator
        /// </summary>		
        public Guid Creator { get; set; }

        /// <summary>
        /// CreationTime
        /// </summary>		
        public DateTime CreationTime { get; set; }
    }
}
