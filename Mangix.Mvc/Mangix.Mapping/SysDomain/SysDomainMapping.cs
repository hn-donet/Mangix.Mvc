﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mangix.Mapping.SysDomain
{
    [Serializable]
 public   class SysDomainMapping
    {
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int DomainType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreationTime { get; set; }


    }
}
