﻿using Mangix.Core.Librs;
using Mangix.Mapping.Category;
using Mangix.Mapping.Setting;
using Mangix.Mapping.SysDomain;
using Mangix.Mapping.SysPermission;
using Mangix.Mapping.SysRole;
using Mangix.Mapping.SysStore;
using Mangix.Mapping.SysUser;
using Mangix.Mapping.SysUserLoginLog;
using Mangix.Mapping.SysUserToken;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mangix.Mapping
{
   public static class MappingExtensions
    {
        private static TDestination mapTo<TSource, TDestination>(this TSource source)
        {
            return MapperManager.Map<TSource, TDestination>(source);
        }
         

        #region SysUser

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static Entities.SysUser toEntity(this SysUserMapping model)
        {
            return mapTo<SysUserMapping, Entities.SysUser>(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static SysUserMapping toModel(this Entities.SysUser source)
        {
            return mapTo<Entities.SysUser,SysUserMapping>(source);
        }
        #endregion

        #region SysRole

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static Entities.SysRole toEntity(this SysRoleMapping model)
        {
            return mapTo<SysRoleMapping, Entities.SysRole>(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static SysRoleMapping toModel(this Entities.SysRole source)
        {
            return mapTo<Entities.SysRole, SysRoleMapping>(source);
        }
        #endregion

        #region SysUserLoginLog

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static Entities.SysUserLoginLog toEntity(this SysUserLoginLogMapping model)
        {
            return mapTo<SysUserLoginLogMapping, Entities.SysUserLoginLog>(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static SysUserLoginLogMapping toModel(this Entities.SysUserLoginLog source)
        {
            return mapTo<Entities.SysUserLoginLog, SysUserLoginLogMapping>(source);
        }

        #endregion

        #region SysPermission

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static Entities.SysPermission toEntity(this PermissionRecordMapping model)
        {
            return mapTo<PermissionRecordMapping, Entities.SysPermission>(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static PermissionRecordMapping toModel(this Entities.SysPermission source)
        {
            return mapTo<Entities.SysPermission, PermissionRecordMapping>(source);
        }
        #endregion

        #region SysStore

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static Entities.SysStore toEntity(this SysStoreMapping model)
        {
            return mapTo<SysStoreMapping, Entities.SysStore>(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static SysStoreMapping toModel(this Entities.SysStore source)
        {
            return mapTo<Entities.SysStore, SysStoreMapping>(source);
        }
        #endregion

        #region SysUserToken

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static Entities.SysUserToken toEntity(this SysUserTokenMapping model)
        {
            return mapTo<SysUserTokenMapping, Entities.SysUserToken>(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static SysUserTokenMapping toModel(this Entities.SysUserToken source)
        {
            return mapTo<Entities.SysUserToken, SysUserTokenMapping>(source);
        }
        #endregion


        #region SysDomain

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static Entities.SysDomain toEntity(this SysDomainMapping model)
        {
            return mapTo<SysDomainMapping, Entities.SysDomain>(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static SysDomainMapping toModel(this Entities.SysDomain source)
        {
            return mapTo<Entities.SysDomain, SysDomainMapping>(source);
        }
        #endregion

        #region Setting

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static Entities.Setting toEntity(this SettingMapping model)
        {
            return mapTo<SettingMapping, Entities.Setting>(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static SettingMapping toModel(this Entities.Setting source)
        {
            return mapTo<Entities.Setting, SettingMapping>(source);
        }
        #endregion


        #region Category

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static Entities.Category toEntity(this CategoryMapping model)
        {
            return mapTo<CategoryMapping, Entities.Category>(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static CategoryMapping toModel(this Entities.Category source)
        {
            return mapTo<Entities.Category, CategoryMapping>(source);
        }
        #endregion
    }
}
