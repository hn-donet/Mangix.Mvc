﻿using Mangix.Mapping.SysUser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mangix.Mapping.SysUserToken
{
    [Serializable]
    public class SysUserTokenMapping
    {

        /// <summary>
        /// 
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 用户id
        /// </summary>
        public Guid SysUserId { get; set; }

        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime ExpireTime { get; set; }

        #region 扩展字段

        /// <summary>
        /// 用户
        /// </summary>
        public SysUserMapping SysUser { get; set; }

        #endregion
    }
}
