﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Mangix.Mapping.Category
{
    [Serializable]
    public class CategoryMapping
    {
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "名字必填")]
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsMenu { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "资源标识必须")]
        public string SysResource { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ResouceID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FatherResource { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FatherID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Controller { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Action { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string RouteName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CssClass { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsDisabled { get; set; }
    }

}
