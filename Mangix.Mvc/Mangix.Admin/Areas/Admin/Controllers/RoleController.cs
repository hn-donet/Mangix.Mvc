﻿using Mangix.Admin.Framework.Controllers;
using Mangix.Admin.Framework.Menu;
using Mangix.Services.SysRole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mangix.Mapping;
using Mangix.Mapping.SysRole;
using Mangix.Services.SysPermission;
using Mangix.Admin.Framework.Controllers.Admin;
using Mangix.Services.Category;

namespace Mangix.Admin.Area.Admin.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [RoutePrefix("role")]
    public class RoleController : BaseAdminController
    {
        // GET: Role
        private ISysRoleService _sysRoleService;
        private ISysPermissionService _sysPermissionService;
        private ICategoryService _categoryService;

        public RoleController(ISysRoleService sysRoleService,
            ISysPermissionService sysPermissionService,
            ICategoryService categoryService)
        {
            this._sysRoleService = sysRoleService;
            this._sysPermissionService = sysPermissionService;
            this._categoryService = categoryService;
        }

        /// <summary>
        /// 角色列表
        /// </summary>
        /// <returns></returns>
        [Descriper("角色列表", true, "menu-icon fa fa-caret-right", FatherResource = "Mangix.Admin.Area.Admin.Controllers.SystemManageController")]
        [Route("", Name = "roleIndex")]
        public ActionResult RoleIndex(AdminSearchRoleArg arg)
        {
            return View(_sysRoleService.searchRoles(arg));
        }

        /// <summary>
        /// 新增修改角色
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("edit", Name = "editRole")]
        [Descriper("新增修改角色", false, FatherResource = "Mangix.Admin.Area.Admin.Controllers.RoleController.RoleIndex")]
        [HttpGet]
        public ActionResult EditRole(Guid? id)
        {
            SysRoleMapping model = new SysRoleMapping();
            if (id.HasValue && id.Value != Guid.Empty)
            {
                var item = _sysRoleService.getRole(id.Value);
                if (item != null)
                    model = item.toModel();
            }
            return View(model);
        }
        [Route("edit")]
        [HttpPost]
        public ActionResult EditRole(SysRoleMapping model)
        {
            if (!ModelState.IsValid)
                return View(model);
            model.Name = model.Name.Trim();
            if (model.Id == Guid.Empty)
            {
                model.Id = Guid.NewGuid();
                model.CreationTime = DateTime.Now;
                model.Creator = WorkContext.CurrentUser.Id;
                _sysRoleService.InserRole(model);
            }
            else
            {
                model.ModifiedTime = DateTime.Now;
                model.Modifier = WorkContext.CurrentUser.Id;
                _sysRoleService.UpdateRole(model);
            }
            return RedirectToRoute("roleIndex");
        }

        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("delete/{id}", Name = "deleteRole")]
        [Descriper("删除角色", false, FatherResource = "Mangix.Admin.Area.Admin.Controllers.RoleController.RoleIndex")]
        public JsonResult DeleteRole(Guid id)
        {
            _sysRoleService.DeleteRole(id);
            AjaxData.Status = true;
            AjaxData.Message = "角色已删除成功";
            return Json(AjaxData, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 角色权限设置
        /// </summary>
        /// <returns></returns>
        [Route("permission", Name = "rolePermission")]
        [Descriper("角色权限设置", false, FatherResource = "Mangix.Admin.Area.Admin.Controllers.RoleController.RoleIndex")]
        [HttpGet]
        public ActionResult RolePermission(Guid id)
        {
            RolePermissionViewModel model = new RolePermissionViewModel();
            model.CategoryList = _categoryService.getAllCache();
            var roleList = _sysRoleService.getAllRoles();
            if (roleList != null && roleList.Any())
            {
                model.Role = roleList.FirstOrDefault(o => o.Id == id);
                model.RoleList = roleList;
                model.Permissions = _sysPermissionService.getByRoleId(id);
            }
            return View(model);
        }
         
        [HttpPost]
        [Route("permission")]
        public JsonResult RolePermission(Guid id,List<int> sysResource)
        {
            _sysPermissionService.SavePermissionRecord(id, sysResource, WorkContext.CurrentUser.Id);
            AjaxData.Status = true;
            AjaxData.Message = "角色权限设置成功";
            return Json(AjaxData, JsonRequestBehavior.DenyGet);
        }
    }
}