﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mangix.Admin.Framework.Menu;
using Mangix.Admin.Framework.Controllers;
using Mangix.Admin.Framework.Controllers.Admin;

namespace Mangix.Admin.Area.Admin.Controllers
{
    [RoutePrefix("home")]
    public class HomeController : BaseAdminController
    {
         

        [Route("", Name = "homeIndex")]
        public ActionResult Index(string returnUrl)
        {
            ViewBag.ReturnUrl = Url.IsLocalUrl(returnUrl) ? returnUrl : Url.RouteUrl("homeMainIndex");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            return View();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Route("main",Name ="homeMainIndex")]
        public ActionResult MainIndex()
        {

            return View();
        }


    }
}