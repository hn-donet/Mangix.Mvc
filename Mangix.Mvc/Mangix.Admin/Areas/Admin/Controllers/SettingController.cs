﻿using Mangix.Admin.Framework.Controllers;
using Mangix.Admin.Framework.Controllers.Admin;
using Mangix.Admin.Framework.Menu;
using Mangix.Services.Setting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mangix.Admin.Area.Admin.Controllers
{
    [RoutePrefix("setting")]
    public class SettingController : BaseAdminController
    {
        // GET: Setting
        private ISettingService _settingService;

        public SettingController(ISettingService settingService)
        {
            this._settingService = settingService;
        }

        /// <summary>
        /// 基数设置
        /// </summary>
        /// <returns></returns>
        [Route("", Name = "settingIndex")]
        [Descriper("基数设置", true, "menu-icon fa fa-caret-right", FatherResource = "Mangix.Admin.Area.Admin.Controllers.SystemManageController")]
        public ActionResult SettingIndex()
        { 
            return View(_settingService.getAllCache());
        }



    }
}