﻿using Mangix.Admin.Framework.Controllers;
using Mangix.Admin.Framework.Controllers.Admin;
using Mangix.Admin.Framework.Menu;
using Mangix.Core.Extensions;
using Mangix.Mapping.SysDomain;
using Mangix.Mapping.SysStore;
using Mangix.Services.Setting;
using Mangix.Services.SysDomain;
using Mangix.Services.SysStore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mangix.Admin.Area.Admin.Controllers
{
    [RoutePrefix("system")]
    [Descriper("系统管理", true, "menu-icon fa fa-desktop")]
    public class SystemManageController : BaseAdminController
    {
        // GET: SystemManager
        // GET: SystemManage
        private ISysStoreService _sysStoreService;
        private ISysDomainService _sysDomainService;
        private ISettingService _settingService;

        public SystemManageController(ISysStoreService sysStoreService,
            ISysDomainService sysDomainService,
            ISettingService settingService)
        {
            this._sysStoreService = sysStoreService;
            this._sysDomainService = sysDomainService;
            this._settingService = settingService;
        }


        /// <summary>
        /// 系统设置
        /// </summary>
        /// <returns></returns>
        [Route("", Name = "systemIndex")]
        [Descriper("系统设置", true, "menu-icon fa fa-caret-right", FatherResource = "Mangix.Admin.Area.Admin.Controllers.SystemManageController")]
        public ActionResult SystemIndex()
        {
            var model = _sysStoreService.getStore();
            return View(model);
        }

        /// <summary>
        /// 系统基础数据设置
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("editStore", Name = "editStore")]
        [Descriper("编辑系统基础设置", false, FatherResource = "Mangix.Admin.Controllers.Area.Admin.SystemManageController.SystemIndex")]
        public JsonResult EditStore(SysStoreMapping model)
        {
            if(!ModelState.IsValid)
            {
                AjaxData.Message = "数据验证未通过";
                return Json(AjaxData, JsonRequestBehavior.DenyGet);
            }
            model.Creator = WorkContext.CurrentUser.Id;
            model.Modifier = WorkContext.CurrentUser.Id;
            _sysStoreService.saveStore(model);
            AjaxData.Status = true;
            AjaxData.Message = "管理系统设置成功";
            return Json(AjaxData, JsonRequestBehavior.DenyGet);
        }

        /// <summary>
        /// 域名设置
        /// </summary>
        /// <returns></returns>
        [ChildActionOnly]
        public PartialViewResult SysDomain()
        {
            var model = _sysDomainService.getAllCache();
            return PartialView(model);
        }

        /// <summary>
        /// 设置站点域名
        /// </summary>
        /// <param name="domainType"></param>
        /// <param name="domain"></param>
        /// <returns></returns>
        [Route("editDomain", Name = "editDomain")]
        [Descriper("编辑域名设置", false, FatherResource = "Baobei.Admin.Areas.Admin.Controllers.SystemManageController.SystemIndex")]
        [HttpPost]
        public JsonResult EditDomain(string wapDomain, string appDomain)
        {
            if (String.IsNullOrEmpty(wapDomain) && String.IsNullOrEmpty(appDomain))
            {
                AjaxData.Message = "请填写域名";
                return Json(AjaxData, JsonRequestBehavior.DenyGet);
            }
            if (!String.IsNullOrEmpty(wapDomain))
            {
                var model = new SysDomainMapping()
                {
                    DomainType = (int)EnumDomainType.移动端,
                    CreationTime = DateTime.Now,
                    Domain = wapDomain.trimSpace().ToLower()
                };
                _sysDomainService.insertDomain(model);
            }
            if (!String.IsNullOrEmpty(appDomain))
            {
                var model = new SysDomainMapping()
                {
                    DomainType = (int)EnumDomainType.APP接口,
                    CreationTime = DateTime.Now,
                    Domain = appDomain.trimSpace().ToLower()
                };
                _sysDomainService.insertDomain(model);
            }
            AjaxData.Status = true;
            AjaxData.Message = "保存完成";
            return Json(AjaxData, JsonRequestBehavior.DenyGet);
        }










    }
}