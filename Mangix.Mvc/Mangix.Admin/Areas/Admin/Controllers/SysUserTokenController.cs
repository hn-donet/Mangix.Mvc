﻿using Mangix.Admin.Framework.Controllers.Admin;
using Mangix.Admin.Framework.Menu;
using Mangix.Services.SysUserToken;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mangix.Admin.Area.Admin.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [RoutePrefix("user/token")]
    public class SysUserTokenController : BaseAdminController
    {
        // GET: Admin/SysUserToken
        private ISysUserTokenService _sysUserTokenService;


        public SysUserTokenController(ISysUserTokenService sysUserTokenService)
        {
            this._sysUserTokenService = sysUserTokenService;
        }

        /// <summary>
        /// 已登陆过的用户token
        /// </summary>
        /// <returns></returns>
        [Route("", Name = "userTokenIndex")]
        [Descriper("登陆用户Token", true, "menu-icon fa fa-caret-right", FatherResource = "Mangix.Admin.Area.Admin.Controllers.SystemManageController")]
        public ActionResult TokenIndex()
        {
            return View(_sysUserTokenService.getAll());
        }

        /// <summary>
        /// 用户退出登陆
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Descriper("登陆用户Token", false, FatherResource = "Mangix.Admin.Controllers.Area.Admin.SysUserTokenController.TokenIndex")]
        [Route("logout", Name = "userTokenLogOut")]
        public JsonResult Logout(Guid id)
        {
            _sysUserTokenService.logOut(id);
            AjaxData.Status = true;
            AjaxData.Message = "操作成功，用户已强制退出登陆";
            return Json(AjaxData, JsonRequestBehavior.AllowGet);
        }

    }
}