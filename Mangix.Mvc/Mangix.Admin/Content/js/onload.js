﻿//加载进度条
(function () {

    var width_load = 0, load_timeout;
    Progress = function () {
        this.setWidth = function (value) {
            width_load = value;
        }
        this.getWidth = function () {
            return width_load;
        }
    };
    Progress.prototype.clear = function () {
        width_load = 0;
        $('#timer').width(width_load + '%');
        clearTimeout(load_timeout);
    }
    Progress.prototype.go = function () {
        width_load += 1 / 8;
        $('#timer').width(width_load + '%');
        load_timeout = setTimeout(Progress.prototype.go, 10);
        if (width_load >= 100)
            Progress.prototype.clear();
    };


})();
//new Progress().go();


$(function () {
     

    //当前显示的iframe
    var setIframe = function () {
        var iframe = $('.J_iframe.On');
        var height = $(window).height() - $('#navbar').height() - $('#breadcrumbs').height();
        var width = $(window).width() - $('#sidebar').width();
        iframe.width(width).height(height);
    };

    $(window).resize(function () {
        setIframe();
    });
    setIframe();

    $('#sidebar-collapse').click(function () {
        //
        var elm = $('#sidebar');
        if ($('#sidebar').hasClass('menu-min')) {
            elm.removeClass('menu-min');
        } else {
            elm.addClass('menu-min');
        }
        setIframe();
    });

    //添加时导航栏滚动
    var neumAddAnimate = function (forward) {
        var menumTabs = $('#menuTabs');
        var menuLeft = menumTabs.position().left;
        var left = $('.roll-right').position().left;
        var curliElm = menumTabs.find('li.active')
        var curLeft = curliElm.position().left;
        var width = curliElm.width();
        if (curLeft - Math.abs(menuLeft) + width > left) {
            menumTabs.animate({ left: '-=' + (curLeft + width - left - Math.abs(menuLeft)) }, 100);
        }
    }



    //菜单点击
    $('.link-href').click(function (e) {
        e.preventDefault();
        new Progress().clear();

        $('#sidebar li').removeClass('active').removeClass('open')
        $(this).parent('li').addClass('active').parents('li').addClass('active open');
        //
        var menuTabs = $('#menuTabs');
        var _id = $(this).data('id');
        var _curElm = menuTabs.find('[data-id="' + _id + '"]');
        if (_curElm.length > 0) {
            _curElm.addClass('active').siblings('li.active').removeClass('active');
            $('#iframe' + _id).addClass('On').siblings('iframe').removeClass('On');
            return;
        }  
        //移除样式
        menuTabs.find('li.active').removeClass('active');
        var liElm = $('<li class="active" data-id="' + _id + '"><a href="javascript:">' + $(this).data('txt') + '<i class="fa fa-times" aria-hidden="true"></i></a></li>').appendTo($('#menuTabs'));
        $('.J_iframe.On').removeClass('On');
        var iframe = $('<iframe id="iframe' + _id + '" class="J_iframe On" src="' + $(this).data('href') + '" frameborder="0"></iframe>').appendTo($('#mainContent'));

        //设置大小
        setIframe();
        //加进度条
        new Progress().go();

        liElm.click(function (e) {
            $(this).addClass('active').siblings('li.active').removeClass('active');
            iframe.addClass('On').siblings('iframe').removeClass('On');
        }).find('.fa').click(function (e) {
            //移除
            e.stopPropagation();
            if (liElm.hasClass('active')) {
                //存在下一个
                if (liElm.next().length > 0) {
                    var nextElm = liElm.next();
                    nextElm.addClass('active');
                    $('#iframe' + nextElm.data('id')).addClass('On');
                } else {
                    liElm.prev().addClass('active');
                    $('#iframe' + liElm.prev().data('id')).addClass('On');
                     
                }
            }
            iframe.remove();
            liElm.remove();
        });

        //滚动
        neumAddAnimate();



        //页面加载完成后清除进度条
        iframe.on('load', function () {
            new Progress().clear();
        });
    });
    //
    $('#tabsHome').click(function () {
        $(this).addClass('active').siblings('li.active').removeClass('active');
        $('#iframe' + $(this).data('id')).addClass('On').siblings('iframe').removeClass('On');
    });
    //左侧按钮
    $('.roll-left').click(function (e) {
        var left = $('#menuTabs').position().left;
        //
        var btnRight = $('.roll-right').position().left;
        if (left < 0) {
            if (Math.abs(left) > btnRight) {
                $('#menuTabs').animate({ left: '+=' + (btnRight) });
            } else {
                $('#menuTabs').animate({ left: '+=' + Math.abs(left) });
            }
        }
    });
    //右侧按钮
    $('.roll-right').click(function (e) {
        //最后一个元素
        var lastLeft = $('#menuTabs').find('li').last().position().left;
        var width = $('#menuTabs').find('li').last().width();
        var btnRight = $('.roll-right').position().left;
        var left = $('#menuTabs').position().left;
        if (lastLeft + width - Math.abs(left) > btnRight) {
            //
            if (lastLeft + width - Math.abs(left) - btnRight <= btnRight) {
                $('#menuTabs').animate({ left: '-=' + (lastLeft + width - Math.abs(left) - btnRight) });
            } else {
                $('#menuTabs').animate({ left: '-=' + btnRight });
            }
        }
    });

    //关闭全部选项卡
    $('.tabCloseAll').click(function () {
        //
        $('#menuTabs li').not('#tabsHome').remove();
        $('#tabsHome').addClass('active');
        //
        $('iframe.J_iframe').not('#iframeHome').remove();
        $('#iframeHome').addClass('On');
    });

    //关闭其他选项卡
    $('.tabCloseOther').click(function () {
        //
        $('#menuTabs li').not('#tabsHome,.active').remove();
        //
        $('iframe.J_iframe').not('#iframeHome,.On').remove();
    });
     

});



























